﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Configuration;
using System.IO;

namespace Kfh.AzListBlobs
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args == null || args.Length == 0)
            {
                Console.WriteLine("Usage: Kfh.AzListBlobs [pathToOutputFile]");
                return;
            }


            Console.WriteLine("{0} - Started: type Ctrl+c or 'exit' at any time to cancel ...", DateTime.Now);
            MainAsync(args[0]);

            string exit = null;
            while (exit != "exit")
            {
                exit = Console.ReadLine();
            }
        }

        private async static void MainAsync(string outputFile)
        {

            string connectionString = ConfigurationManager.AppSettings["BlobConnectionString"];
            string containerName = ConfigurationManager.AppSettings["BlobContainerName"];

            using (StreamWriter outputWriter = new StreamWriter(outputFile)) {
                {
                    BlobContainer container = new BlobContainer(connectionString, containerName, outputWriter);
                    await container.CountBlobsAsync().ConfigureAwait(false);
                }
            }

            Console.WriteLine("Done!");

        }
    }
}
