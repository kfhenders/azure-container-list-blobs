# Kfh.AzListBlobs

## Overview
Kfh.AzListBlobs is a command line utility that will output the list of blob names contained in an Azure storage container.

## Usage
To use, first populate the app.config with information about the container your are querying.

Kfh.AzListBlobs \[pathToOutputFile\]

