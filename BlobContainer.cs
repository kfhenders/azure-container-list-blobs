﻿/*
Copyright 2015, Kevin Henderson

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Kfh.AzListBlobs
{
    public class BlobContainer 
    {
        private CloudStorageAccount _storageAccount;
        private CloudBlobClient _blobClient;
        private CloudBlobContainer _container;
        private DateTime _startTime;
        private long _totalCount = 0;
        private long _logInterval = 10000;
        private long _nextLogInterval = 10000;
        private StreamWriter _outputWriter;

        public BlobContainer(string connectionString, string containerName, StreamWriter outputWriter)
        {
            _storageAccount = CloudStorageAccount.Parse(connectionString);
            _blobClient = _storageAccount.CreateCloudBlobClient();
            _container = _blobClient.GetContainerReference(containerName);
            _outputWriter = outputWriter;
        }

        public async Task CountBlobsAsync()
        {
            _startTime = DateTime.Now;
            BlobResultSegment resultSegment = await _container.ListBlobsSegmentedAsync(null).ConfigureAwait(false);

            while (resultSegment != null && resultSegment.ContinuationToken != null)
            {
                var asyncResult = _container.ListBlobsSegmentedAsync(resultSegment.ContinuationToken);

                await Write(resultSegment).ConfigureAwait(false);

                resultSegment = await asyncResult;
            }

            await Write(resultSegment, true).ConfigureAwait(false);
        }

        private async Task Write(BlobResultSegment resultSegment, bool lastWrite = false)
        {
            if (resultSegment != null)
            {
                IEnumerable<CloudBlockBlob> blobs =
                    (from b in
                         resultSegment.Results
                     select b as CloudBlockBlob);
                foreach (var blob in blobs)
                {
                    await _outputWriter.WriteLineAsync(blob.Name).ConfigureAwait(false);
                }
                _totalCount += resultSegment.Results.Count();
            }
            if (_totalCount >= _nextLogInterval || lastWrite)
            {
                TimeSpan elapsed = DateTime.Now - _startTime;
                _nextLogInterval += _logInterval;
                Console.WriteLine(String.Format("Count: {0}; Elapsed: {1} - {2}/s", String.Format("{0:#,##0}", _totalCount), elapsed.ToString(@"hh\:mm\:ss"),
                    ((double)_totalCount / elapsed.TotalSeconds).ToString("F2")));
            }

        }

    }
}
